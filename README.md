# Tiện ích Nhiệm Vụ Skin free

Đây là một tiện ích nhỏ và đơn giản giúp kiểm tra những thứ mà bạn tìm kiếm trên google. Sau đó khi bạn làm nhiệm vụ, sẽ giúp trang web đích kiểm tra xem bạn đã tìm kiếm đúng chưa.

## Cách hoạt động
- Khi bạn vào google, tiện ích sẽ phân tích vào lưu lại từ khoá của bạn (không gửi thông tin này đi bất kì đâu)
- Lấy tất cả nhiệm vụ dành cho bạn từ hệ thống
- Khi bạn vào web nằm trong danh sách nhiệm vụ:
-- Lấy từ khoá tìm kiếm cuối cùng của bạn
-- Chèn đoạn mã tracking (hoạt động giống như google analytics) vào trang web nhiệm vụ, đoạn mã này sẽ theo dõi bạn cuộn chuột, thời gian đọc của bạn trên web nhiệm vụ.

## Quyền riêng tư
- Tiện ích sử dụng cookie của lmsssplus
- Tiện ích theo dõi các thao tác ẩn danh bao gồm cuộn chuột, thời gian đọc khi bạn vào web nhiệm vụ
