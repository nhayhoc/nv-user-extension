let keyword: string;

function InjectKeywordVariableAndScript(keyword: string) {
  const keyword_data = document.createElement("div");
  keyword_data.setAttribute("id", "__nv_keyword");
  keyword_data.setAttribute("data-keyword", keyword);

  document.body.appendChild(keyword_data);
}
function InjectExtentionInstall() {
  const keyword_data = document.createElement("div");
  keyword_data.setAttribute("id", "__nv_extension_install");
  document.body.appendChild(keyword_data);
}

chrome.tabs.onUpdated.addListener(async (tabId, changeInfo, tab) => {
  try {
    const url = tab.url;
    console.log({ url, status: changeInfo.status });
    if (tab.active) {
      if (!url) return;
      // check if is google.com
      if (/https?:\/\/.*?google\.com.*?\/search\?q=/gm.test(url)) {
        console.log("google.com");
        // get domain and params

        const m = url.match(/q=.*?&/gm);
        console.log({ m });
        if (!m) return;

        const params = new URLSearchParams(m[0]);
        const _keyword = params.get("q");
        if (!_keyword) throw new Error("keyword is null");
        keyword = _keyword;
        // chrome.runtime.sendMessage({ keyword });

        return;
      }
    }

    if (changeInfo.status != "complete" || !tab.active) {
      return;
    }
    if (!url || !url.startsWith("http")) {
      return;
    }
    await chrome.scripting.executeScript({
      target: { tabId },
      func: InjectExtentionInstall,
    });

    await chrome.scripting.executeScript({
      target: { tabId },
      func: InjectKeywordVariableAndScript,
      args: [keyword || ""],
    });
  } catch (error) {
    console.error(error);
  }
});
